package com.devcamp.user.services;

import com.devcamp.user.models.Product;
import com.devcamp.user.repository.ProductRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {
    private final ProductRepository productRepository;

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }

    public Product getProductById(Long id) {
        Optional<Product> optionalProduct = productRepository.findById(id);
        return optionalProduct.orElse(null);
    }

    public Product createProduct(Product product) {
        return productRepository.save(product);
    }

    public Product updateProduct(Long id, Product product) {
        Optional<Product> optionalProduct = productRepository.findById(id);
        if (optionalProduct.isPresent()) {
            Product existingProduct = optionalProduct.get();
            existingProduct.setProductCode(product.getProductCode());
            existingProduct.setProductName(product.getProductName());
            existingProduct.setProductDescripttion(product.getProductDescripttion());
            existingProduct.setProductLine(product.getProductLine());
            existingProduct.setProductScale(product.getProductScale());
            existingProduct.setProductVendor(product.getProductVendor());
            existingProduct.setQuantityInStock(product.getQuantityInStock());
            existingProduct.setBuyPrice(product.getBuyPrice());
            existingProduct.setOrderDetails(product.getOrderDetails());
            return productRepository.save(existingProduct);
        } else {
            return null;
        }
    }

    public void deleteProduct(Long id) {
        productRepository.deleteById(id);
    }
}
