package com.devcamp.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.user.models.Office;

public interface OfficeRepository extends JpaRepository<Office, Long> {

}
