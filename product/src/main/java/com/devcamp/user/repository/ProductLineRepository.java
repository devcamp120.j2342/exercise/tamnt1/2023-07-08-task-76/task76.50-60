package com.devcamp.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.user.models.ProductLine;

public interface ProductLineRepository extends JpaRepository<ProductLine, Long> {

}
