package com.devcamp.user.services;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import com.devcamp.user.models.Customer;
import com.devcamp.user.repository.CustomerRepository;

@Service
public class CustomerService {
    private final CustomerRepository customerRepository;

    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public List<Customer> getAllCustomers() {
        return customerRepository.findAll();
    }

    public Customer getCustomerById(int id) {
        Optional<Customer> optionalCustomer = customerRepository.findById(id);
        return optionalCustomer.orElse(null);
    }

    public Customer createCustomer(Customer customer) {
        return customerRepository.save(customer);
    }

    public Customer updateCustomer(int id, Customer customer) {
        Optional<Customer> optionalCustomer = customerRepository.findById(id);
        if (optionalCustomer.isPresent()) {
            Customer existingCustomer = optionalCustomer.get();
            existingCustomer.setLastName(customer.getLastName());
            existingCustomer.setFirstName(customer.getFirstName());
            existingCustomer.setPhoneNumber(customer.getPhoneNumber());
            existingCustomer.setAddress(customer.getAddress());
            existingCustomer.setCity(customer.getCity());
            existingCustomer.setState(customer.getState());
            existingCustomer.setPostalCode(customer.getPostalCode());
            existingCustomer.setCountry(customer.getCountry());
            existingCustomer.setSalesRepEmployeeNumber(customer.getSalesRepEmployeeNumber());
            existingCustomer.setCreditLimit(customer.getCreditLimit());
            existingCustomer.setOrders(customer.getOrders());
            existingCustomer.setPayments(customer.getPayments());
            return customerRepository.save(existingCustomer);
        } else {
            return null;
        }
    }

    public void deleteCustomer(int id) {
        customerRepository.deleteById(id);
    }
}
