package com.devcamp.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.user.models.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {

}
