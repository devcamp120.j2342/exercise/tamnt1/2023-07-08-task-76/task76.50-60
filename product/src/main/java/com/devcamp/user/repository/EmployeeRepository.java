package com.devcamp.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.devcamp.user.models.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {

}