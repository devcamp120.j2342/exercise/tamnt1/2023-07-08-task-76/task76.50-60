package com.devcamp.user.services;

import com.devcamp.user.models.ProductLine;
import com.devcamp.user.repository.ProductLineRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductLineService {
    private final ProductLineRepository productLineRepository;

    public ProductLineService(ProductLineRepository productLineRepository) {
        this.productLineRepository = productLineRepository;
    }

    public List<ProductLine> getAllProductLines() {
        return productLineRepository.findAll();
    }

    public ProductLine getProductLineById(Long id) {
        Optional<ProductLine> optionalProductLine = productLineRepository.findById(id);
        return optionalProductLine.orElse(null);
    }

    public ProductLine createProductLine(ProductLine productLine) {
        return productLineRepository.save(productLine);
    }

    public ProductLine updateProductLine(Long id, ProductLine productLine) {
        Optional<ProductLine> optionalProductLine = productLineRepository.findById(id);
        if (optionalProductLine.isPresent()) {
            ProductLine existingProductLine = optionalProductLine.get();
            existingProductLine.setProductLine(productLine.getProductLine());
            existingProductLine.setDescription(productLine.getDescription());
            existingProductLine.setProducts(productLine.getProducts());
            return productLineRepository.save(existingProductLine);
        } else {
            return null;
        }
    }

    public void deleteProductLine(Long id) {
        productLineRepository.deleteById(id);
    }
}
